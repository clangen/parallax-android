package org.clangen.gfx.parallax;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SettingsActivity extends Activity {
    private static final String THEME_DIRECTORY = "/sdcard/.parallax/themes/";
    public static final String ACTION_SETTINGS_STARTED = "org.clangen.gfx.parallax.ACTION_SETTINGS_STARTED";
    public static final String ACTION_SETTINGS_FINISHED = "org.clangen.gfx.parallax.ACTION_SETTINGS_FINISHED";
    public static final String ACTION_SETTINGS_PRIORITY_CHANGED = "org.clangen.gfx.parallax.ACTION_PRIORITY_CHANGED";

    private static volatile boolean sIsActive = false;

    private Theme mTheme;
    private boolean mPaused = true;
    private Parallax mParallax;
    private ListView mListView;
    private View mNoThemesTextView;
    private SurfaceView mSurfaceView;
    private ArrayAdapter<String> mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTheme = Theme.getInstance(this);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings);

        mParallax = new Parallax(this);

        mSurfaceView = (SurfaceView) findViewById(R.id.SurfaceView);
        mSurfaceView.getHolder().addCallback(mSurfaceHolderCallback);

        mListView = (ListView) findViewById(R.id.ThemesListView);
        mListView.setOnItemClickListener(mOnProfileRowClickListener);

        mNoThemesTextView = findViewById(R.id.NoThemesTextView);

        findViewById(R.id.OKButton).setOnClickListener(mDoneClickListener);

        new ConfigScanner().execute((Void[]) null);

        setNoThemesVisibility(true);
    }

    public static boolean isActive() {
        return sIsActive;
    }

    private void setNoThemesVisibility(boolean visible) {
        mNoThemesTextView.setVisibility(visible ? View.VISIBLE : View.GONE);
        mListView.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    protected void onResume() {
        super.onResume();
        sIsActive = true;
        mPaused = false;

        sendBroadcast(new Intent(ACTION_SETTINGS_STARTED));
    }

    protected void onPause() {
        super.onPause();
        sIsActive = false;
        mPaused = true;
        sendBroadcast(new Intent(ACTION_SETTINGS_FINISHED));
    }

    private void onEffectChanged() {
        mParallax.onEffectChanged();
        mParallax.moveTo(0.0f);
    }

    private OnItemClickListener mOnProfileRowClickListener =
        new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                String themeName = mListAdapter.getItem(index);
                mTheme.setThemePath(THEME_DIRECTORY + themeName);
                onEffectChanged();
            }
    };

    private OnClickListener mDoneClickListener = new OnClickListener() {
        public void onClick(View view) {
            finish();
        }
    };

    private SurfaceHolder.Callback mSurfaceHolderCallback = new SurfaceHolder.Callback() {
        public void surfaceDestroyed(SurfaceHolder holder) {
            mParallax.stop();
        }

        public void surfaceCreated(SurfaceHolder holder) {
            // surfaceChanged() always called at least once after created(), start it there.
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (holder.getSurface().isValid()) {
                mParallax.start(holder);
                mParallax.moveTo(0.0f);
            }
        }
    };

    private class ConfigScanner extends AsyncTask<Void, Void, Void> {
        private ProgressDialog mProgressDialog;
        private ArrayList<String> mThemes = new ArrayList<String>();

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if ( ! mPaused && mProgressDialog != null) {
                mProgressDialog.dismiss();
            }

            if (mThemes.size() > 0) {
                mListAdapter = new ArrayAdapter<String>(
                    SettingsActivity.this,
                    R.layout.themerow,
                    R.id.ThemeRowName,
                    mThemes);

                mListView.setAdapter(mListAdapter);
            }

            setNoThemesVisibility(mThemes.size() == 0);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if ( ! mPaused) {
                mProgressDialog =
                    ProgressDialog.show(
                        SettingsActivity.this,
                        getString(R.string.theme_init_title),
                        getString(R.string.theme_init_message));
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            synchronized (ConfigScanner.class) {
                File themeDir = new File(THEME_DIRECTORY);

                themeDir.mkdirs();

                if (themeDir.exists() && themeDir.isDirectory()) {
                    final ArrayList<String> themes = new ArrayList<String>();

                    themeDir.listFiles(new FileFilter() {
                        public boolean accept(File pathname) {
                            if (pathname.isDirectory()) {
                                themes.add(pathname.getName());
                            }

                            return false;
                        }
                    });

                    mThemes = themes;
                }
            }

            return null;
        }
    }
}
