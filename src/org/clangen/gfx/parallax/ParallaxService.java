package org.clangen.gfx.parallax;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

public class ParallaxService extends WallpaperService {

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private class Engine extends WallpaperService.Engine {
        private Parallax mParallax;
        private boolean mVisible;
        private float mLastOffset;

        public Engine() {
            mVisible = false;
            mParallax = new Parallax(ParallaxService.this);
            setTouchEventsEnabled(true);
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            registerReceivers();
        }

        @Override
        public void onDestroy() {
            unregisterReceivers();
            super.onDestroy();
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                float xOffsetStep, float yOffsetStep, int xPixelOffset,
                int yPixelOffset)
        {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep,
                    xPixelOffset, yPixelOffset);

            mLastOffset = xOffset;
            mParallax.moveTo(xOffset);
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            if (mVisible && ( ! SettingsActivity.isActive())) {
                start();
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            mVisible = visible;

            if (visible) {
                if ( ! SettingsActivity.isActive()) {
                    start();
                }
            }
            else {
                stop();
            }
        }

        private synchronized void start() {
            mParallax.start(getSurfaceHolder());
            mParallax.moveTo(mLastOffset);
        }

        private synchronized void stop() {
            mParallax.stop();
        }

        private void registerReceivers() {
            registerReceiver(
                mOnSettingsFinishedReceiver,
                new IntentFilter(SettingsActivity.ACTION_SETTINGS_FINISHED));

            registerReceiver(
                mOnEffectChangedReceiver,
                new IntentFilter(Theme.ACTION_EFFECT_CHANGED));
        }

        private void unregisterReceivers() {
            unregisterReceiver(mOnSettingsFinishedReceiver);
            unregisterReceiver(mOnEffectChangedReceiver);
        }

        private BroadcastReceiver mOnEffectChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (mParallax != null) {
                    mParallax.onEffectChanged();
                }
            }
        };

        private BroadcastReceiver mOnSettingsFinishedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (mVisible) {
                    start();
                }
            }
        };
    }
}
