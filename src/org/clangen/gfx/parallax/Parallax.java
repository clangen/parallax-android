package org.clangen.gfx.parallax;

import java.io.Closeable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;

public class Parallax {
    private static final String TAG = "Parallax";

    private static final int MESSAGE_BASE = 0xdeadbeef;
    private static final int MESSAGE_DRAW = MESSAGE_BASE + 1;
    private static final int MESSAGE_QUIT = MESSAGE_BASE + 2;

    private static LinkedList<Layer> LAYERS;
    private static String THEME = "";

    private Context mContext;
    private DrawQueue mDrawQueue;
    private Theme mTheme;
    private SurfaceHolder mSurfaceHolder;

    private int mScreenWidth;
    private int mScreenHeight;

    private class Layer implements Closeable {
        private Bitmap mBitmap;
        private int mMaxOffset;

        public Layer(Bitmap bitmap) {
            if (bitmap.getHeight() != mScreenHeight) {
                float scaleRatio = (float) mScreenHeight / (float) bitmap.getHeight();

                Matrix matrix = new Matrix();
                matrix.postScale(scaleRatio, scaleRatio);

                Bitmap newBitmap = Bitmap.createBitmap(
                    bitmap,
                    0,
                    0,
                    bitmap.getWidth(),
                    bitmap.getHeight(),
                    matrix,
                    true);

                bitmap.recycle();
                bitmap = newBitmap;
            }

            mBitmap = bitmap;
            mMaxOffset = mBitmap.getWidth() - mScreenWidth;
        }

        public void close() {
            if (mBitmap != null) {
                mBitmap.recycle();
                mBitmap = null;
            }
        }

        public void drawWithOffset(Canvas canvas, float offset) {
            int yOffsetPx = (int) ((float) mMaxOffset * offset);

            Rect src = new Rect(yOffsetPx, 0, yOffsetPx + mScreenWidth, mScreenHeight);
            Rect dst = new Rect(0, 0, mScreenWidth, mScreenHeight);

            canvas.drawBitmap(mBitmap, src, dst, null);
        }
    }

    public Parallax(Context context) {
        mContext = context.getApplicationContext();
        mTheme = Theme.getInstance(mContext);
    }

    public synchronized void moveTo(float newOffset) {
        if (mDrawQueue != null) {
            mDrawQueue.sendDraw(newOffset);
        }
    }

    public synchronized void onEffectChanged() {
        if (mDrawQueue != null) {
            restartIfRunning();
        }
    }

    public synchronized boolean start(SurfaceHolder surfaceHolder) {
        stop();

        if (surfaceHolder != null) {
            mSurfaceHolder = surfaceHolder;
            init();
        }

        return (mDrawQueue != null);
    }

    public synchronized void stop() {
        if (mDrawQueue != null) {
            mDrawQueue.cancel();
            mDrawQueue = null;
        }

        mSurfaceHolder = null;
    }

    private void restartIfRunning() {
        if (mSurfaceHolder != null) {
            start(mSurfaceHolder);
        }
    }

    private void reloadSelectedTheme() {
        synchronized (Parallax.class) {
            String theme = mTheme.getThemePath();
            if ((theme != null) && (!theme.equals(THEME))) {
                Log.i(TAG, "Loading theme: " + theme);

                if (LAYERS != null) {
                    for (Layer layer : LAYERS) {
                        layer.close();
                    }

                    /*
                     *  bleh. if the themes are changed too frequently we can get an
                     *  OOM exception because the GC hasn't kicked in to free the old
                     *  bitmaps yet. the two GCs below mitigate the issue slightly.
                     */
                    System.gc();
                    System.gc();
                }

                LAYERS = new LinkedList<Layer>();
                List<Bitmap> bitmaps = mTheme.getBitmaps();
                for (Bitmap b : bitmaps) {
                    LAYERS.add(new Layer(b));
                }

                THEME = theme;
            }
        }
    }

    private void calculateDimensions() {
        synchronized (Parallax.class) {
            WindowManager wm = (WindowManager)
            mContext.getSystemService(Service.WINDOW_SERVICE);

            int width = wm.getDefaultDisplay().getWidth();
            int height = wm.getDefaultDisplay().getHeight();

            if (mScreenWidth != width || mScreenHeight != height) {
                mScreenWidth = width;
                mScreenHeight = height;
                THEME = null; // triggers theme reload if dimensions change
            }
        }
    }

    private void init() {
        Canvas canvas = mSurfaceHolder.lockCanvas();

        if (canvas != null) {
            try {
                calculateDimensions();
                reloadSelectedTheme();

                mDrawQueue = new DrawQueue();
                mDrawQueue.start();
                mDrawQueue.waitForStart();
            }
            finally {
                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    private void drawWithOffset(Canvas canvas, float offset) {
        synchronized (Parallax.class) {
            if (LAYERS != null) {
                for (Layer layer : LAYERS) {
                    layer.drawWithOffset(canvas, offset);
                }
            }
        }
    }

    private class DrawQueue extends Thread {
        private Looper mLooper;
        private Handler mHandler;
        private CountDownLatch mStopLatch, mStartLatch;

        public DrawQueue() {
            mStopLatch = new CountDownLatch(1);
            mStartLatch = new CountDownLatch(1);
            setName("Parallax.DrawQueue");
        }

        public void cancel() {
            if (mLooper != null) {
                mHandler.removeMessages(MESSAGE_DRAW);
                mHandler.sendMessageAtFrontOfQueue(newQuitMessage());

                try {
                    mStopLatch.await();
                    mLooper = null;
                }
                catch (InterruptedException ex) {
                    Log.i(TAG, "DrawQueue.cancel latch await interrupted??");
                }
            }
        }

        public void waitForStart() {
            try {
                mStartLatch.await();
            }
            catch (InterruptedException ex) {
                Log.i(TAG, "DrawQueue.waitForStart latch await interrupted??");
            }
        }

        public void sendDraw(float offset) {
            mHandler.sendMessage(newDrawMessage(offset));
        }

        public void run() {
            Looper.prepare();
            mLooper = Looper.myLooper();

            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == MESSAGE_QUIT) {
                        mLooper.quit();
                    }
                    else {
                        SurfaceHolder holder = mSurfaceHolder;
                        if (holder != null) {
                            Canvas canvas = holder.lockCanvas();

                            if (canvas != null) {
                                if (msg.what == MESSAGE_DRAW) {
                                    drawWithOffset(canvas, (Float) msg.obj);
                                }

                                holder.unlockCanvasAndPost(canvas);
                            }
                        }
                    }
                }
            };

            mStartLatch.countDown();
            Looper.loop();
            mStopLatch.countDown();
        }

        private Message newQuitMessage() {
            Message message = mHandler.obtainMessage();
            message.what = MESSAGE_QUIT;
            return message;
        }

        private Message newDrawMessage(float offset) {
            Message message = mHandler.obtainMessage();
            message.what = MESSAGE_DRAW;
            message.obj = Float.valueOf(offset);
            return message;
        }
    }
}