package org.clangen.gfx.parallax;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.WindowManager;

public class Theme {
    public static final String ACTION_EFFECT_CHANGED = "org.clangen.gfx.parallax.ACTION_SETTINGS_CHANGED";

    private static Theme sInstance;

    private Application mContext;
    private SharedPreferences mPrefs;

    private Theme(Context context) {
        mContext = (Application) context.getApplicationContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static Theme getInstance(Context context) {
        synchronized (Theme.class) {
            if (sInstance == null) {
                sInstance = new Theme(context.getApplicationContext());
            }

            return sInstance;
        }
    }

    public void setThemePath(String path) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(mContext.getString(R.string.pref_last_theme_path), path);
        editor.commit();

        mContext.sendBroadcast(new Intent(ACTION_EFFECT_CHANGED));
    }

    public String getThemePath() {
        return mPrefs.getString(mContext.getString(R.string.pref_last_theme_path), null);
    }

    public List<Bitmap> getBitmaps() {
        String themePath = getThemePath();
        ArrayList<Bitmap> result = new ArrayList<Bitmap>();

        if (themePath != null) {
            File dir = new File(themePath);

            if (dir.exists() && dir.isDirectory()) {
                // determine image filenames
                final TreeSet<String> all = new TreeSet<String>();
                final TreeSet<String> portrait = new TreeSet<String>();
                final TreeSet<String> landscape = new TreeSet<String>();

                dir.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String filename) {
                        String fullPath = dir.getAbsolutePath() + "/" + filename;
                        String fn = filename.toLowerCase();
                        if (fn.endsWith(".png")) {
                            if(fn.endsWith(".portrait.png")) {
                                portrait.add(fullPath);
                            }
                            else if (fn.endsWith(".landscape.png")) {
                                landscape.add(fullPath);
                            }

                            all.add(fullPath);
                        }

                        return false;
                    }
                });

                // determine orientation, image set
                WindowManager wm = (WindowManager)
                    mContext.getSystemService(Service.WINDOW_SERVICE);

                Display display = wm.getDefaultDisplay();

                boolean isPortrait = (display.getHeight() > display.getWidth());

                TreeSet<String> images = all;

                if (isPortrait && (portrait.size() > 0)) {
                    images = portrait;
                }
                else if (!isPortrait && (landscape.size() > 0)) {
                    images = landscape;
                }

                // load bitmaps
                for (String filename : images) {
                    result.add(BitmapFactory.decodeFile(filename));
                }
            }
        }

        return result;
    }
}
